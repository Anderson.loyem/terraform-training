resource "local_file" "pet" {
  filename = "./pet.txt"
  content = "tetst"
  file_permission = 0700
}

resource "random_pet" "my-pet" {
  prefix = "Mrs"
  separator = ","
  length = "1"
}