module "my-vm" {
    source = "../modules/ec2"
    size_instance = "t2.micro"
    tags = {
    "env"  = "dev"
    "Name" = "ec2-loyem"
  }
}

output "my-vm" {
    value = {
        "AZ" = module.my-vm.instance.availability_zone, 
        "IP_P" =module.my-vm.instance.public_ip
    }
}