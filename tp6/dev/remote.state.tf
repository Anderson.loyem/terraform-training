terraform {
  backend "s3" {
    bucket = "terraform-backend-loyem"
    key    = "tf_module_backend.tfstate"
    region = "eu-west-3"
  }
}
