
variable "size_instance" {
  type = string
  description = ""
  default = "t2.nano"
}

variable "tags" {
    type = map
    description = "(optional) describe your variable"
    default = {
        Name = "test"
    }
}