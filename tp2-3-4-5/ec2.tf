provider "aws" {
  region = "eu-west-3"
}


resource "aws_security_group" "my_sg" {
  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my_instance" {
  ami                         = "ami-017d9f576d1635a77"
  instance_type               = var.size_instance
  key_name                    = "devops-loyem"
  vpc_security_group_ids      = [aws_security_group.my_sg.id]
  associate_public_ip_address = true

  provisioner "local-exec" {
    command = "echo ${aws_instance.my_instance.private_ip}, ${aws_instance.my_instance.id}, ${aws_instance.my_instance.availability_zone} >> info_ec2.txt"
  }

#  provisioner "remote-exec" {
#    inline = [
#      "sudo systemctl start nginx"
#    ]
#  }

#  connection {
#    type = "ssh"
#    user = "ec2-user"
#    private_key = file("./devops-loyem.pem")
#    host = self.public_ip
#    timeout = 1
#  }

  tags = {
    "env"  = "dev"
    "Name" = "ec2-loyem"
  }
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

# resource block for eip #
resource "aws_eip" "my_eip" {
  instance = aws_instance.my_instance.id
  vpc      = true
}

resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_default_vpc.default.id

  tags = {
    Name = "main"
  }
}

#Associate EIP with EC2 Instance
resource "aws_eip_association" "my_eip_association" {
  instance_id   = aws_instance.my_instance.id
  allocation_id = aws_eip.my_eip.id
}
